@extends('panel.layout.master' , ['title' => 'المستخدمين'])

@push('css')
    <link href="{{asset('panelAssets/css/bootstrap-select.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('panelAssets/css/fancybox.min.css')}}" rel="stylesheet" type="text/css"/>

@endpush


@section('content_head')
    <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">المستخدمين</h3>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand fa fa-users"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    المستخدمين
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{{ route('panel.users.create') }}" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            إضافة مستخدم
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Search Form -->
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">

                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control"
                                           placeholder="بحث..." id="generalSearch">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--left">
																<span><i class="la la-search"></i></span>
															</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <!--end: Search Form -->
        </div>
        <div class="kt-portlet__body kt-portlet__body--fit">

            <!--begin: Datatable -->
            <div class="kt-datatable text-center" id="ajax_data"></div>

            <!--end: Datatable -->
        </div>
    </div>

@endsection

@push('js')
    <script src="{{asset('panelAssets/js/jquery.dataTables.js')}}" type="text/javascript"></script>
    <script src={{asset('panelAssets/js/data-ajax.js')}}  type="text/javascript"></script>
    <script src={{asset('panelAssets/js/fancybox.min.js')}}  type="text/javascript"></script>


    <script>
        $('.bootstrap-select').selectpicker();

        window.data_url = '{{url('panel/users/datatable')}}';

        window.columns = [
            {
                field: 'avatar',
                title: "الصورة",
                width: 100,
                'class': "text-center",
                textAlign: "center",
                template: function (data) {
                    if (data.avatar) {
                        return '<a href="{{ url('image/') }}/' + data.avatar + '" data-fancybox ><img  src="{{ url('image/') }}/' + data.avatar + '/80x80" style="border-radius:50%" ></a>';
                    } else {
                        return '<a href="{{ url('image/admins/default.jpg') }}" data-fancybox ><img  src="{{ url('image/admins/default.jpg/80x80') }}" style="border-radius:50%" ></a>';
                    }
                }
            },
            {
                field: 'full_name',
                title: "الإسم",
                textAlign: "center",
            },
            {
                field: 'phone',
                title: "رقم الجوال",
                textAlign: "center",
            }, {
                field: 'email',
                title: "البريد الإلكتروني",
                textAlign: "center",

            }, {
                field: 'created_at',
                title: "تاريخ الإنضمام",
                textAlign: "center",
            },
            {
                field: 'Actions',
                title: "العمليات",
                sortable: false,
                overflow: 'visible',
                textAlign: "center",
                autoHide: false,
                width: 100,
                template: function (data) {
                    var editUrl = "{{ url('panel/users/') }}/" + data.id + "/edit";
                    var deleteUrl = "{{ url('panel/users/') }}/" + data.id;
                    return `
                        <input value=` + data.id + ` type="hidden" class="id">
						<div class="dropdown">
							<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown">
                                <i class="la la-cog"></i>
                            </a>
						  	<div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="` + editUrl + `"><i class="la la-edit"></i> تعديل</a>
                                    <a class="dropdown-item delete" href="#" data-url="` + deleteUrl + `"><i class="la la-times"></i> حذف </a>
						  	</div>
						</div>`;
                },
            }
        ];

        $('#search').on('click', function () {

            var query = datatable.getDataSourceQuery();
            query.name = $('#name').val().toLowerCase();
            query.email = $('#email').val().toLowerCase();
            query.phone = $('#phone').val().toLowerCase();
            query.ssn = $('#ssn').val().toLowerCase();

            datatable.setDataSourceQuery(query);
            datatable.load();
        });
        $('#cancelSearch').on('click', function () {
            $('form#searchForm').resetForm();
            $(".bootstrap-select").selectpicker("refresh");

            var query = datatable.getDataSourceQuery();
            query.name = $('#name').val().toLowerCase();
            query.email = $('#email').val().toLowerCase();
            query.phone = $('#phone').val().toLowerCase();
            query.ssn = $('#ssn').val().toLowerCase();

            datatable.setDataSourceQuery(query);
            datatable.load();
        });

    </script>

    <script>

    </script>
@endpush
