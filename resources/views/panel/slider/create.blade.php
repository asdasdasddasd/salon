@extends('panel.layout.master' , ['title' => 'إضافة سلايدر'])



@section('content_head')
    <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">إضافة سلايدر</h3>
            </div>
        </div>
    </div>

@endsection

@section('content')

    <form class="kt-form" action="{{ route('panel.slider.store') }}" method="post" enctype="multipart/form-data" id="kt_form_1">
        @csrf
        <div class="row">

            <div class="col-md-9">

                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                إضافة سلايدر
                            </h3>
                        </div>
                    </div>

                    <!--begin::Form-->

                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <label>عنوان السلايدر</label>
                            <input type="text" class="form-control" name="title" value="{{ old('title') }}" placeholder="عنوان السلايدر">
                        </div>

                        <div class="form-group">
                            <label>وصف السلايدر</label>
                            <textarea class="form-control" name="description" placeholder="وصف السلايدر">{{ old('description') }}</textarea>
                        </div>


                        <div class="form-group">
                            <label class="kt-checkbox">
                                <input type="checkbox" {{  old('isVisible') == "on" ? "checked" : ""  }} name="isVisible"> قابل للعرض
                                <span></span>
                            </label>
                        </div>
                    </div>

                    <!--end::Form-->
                </div>

                <!--end::Portlet-->

            </div>

            <div class="col-md-3">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                إضافة سلايدر
                            </h3>
                        </div>
                    </div>

                    <!--begin::Form-->


                    <div class="kt-portlet__body ">


                        <div class="form-group">
                            <label>صورة السلايدر</label>
                            <div></div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="imgload" name="image">
                                <label class="custom-file-label" for="imgload">Choose file</label>
                            </div>
                        </div>

                        <div class="img-responsive">
                            <div class="imageEditProfile">
                                <img src="" alt="" id="imgshow" style="max-width: 100%">
                            </div>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" class="btn btn-success">حفظ</button>
                        </div>
                    </div>


                    <!--end::Form-->
                </div>
                <!--end::Portlet-->

            </div>

        </div>
    </form>
@endsection


@push('js')

    <script>

        var KTFormControls = function () {
            // Private functions

            var demo1 = function () {
                $( "#kt_form_1" ).validate({
                    // define validation rules
                    rules: {
                        title: {
                            required: true,
                        },
                        description: {
                            required: true
                        },
                        image: {
                            required: true
                        },
                    },

                    //display error alert on form submit
                    invalidHandler: function(event, validator) {
                        var alert = $('#kt_form_1_msg');
                        alert.removeClass('kt--hide').show();
                        KTUtil.scrollTop();
                    },

                    submitHandler: function (form) {
                        form[0].submit(); // submit the form
                    }
                });
            }




            return {
                // public functions
                init: function() {
                    demo1();
                }
            };
        }();

        jQuery(document).ready(function() {
            KTFormControls.init();
        });
    </script>
    <script>
        $("#imgload").change(function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imgshow').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });

    </script>
@endpush
