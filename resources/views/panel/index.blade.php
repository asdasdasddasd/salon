@extends('panel.layout.master' , ['title' => 'الرئيسية'])

@section('content_head')
    <div class="kt-subheader  kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">لوحة التحكم</h3>
            </div>
        </div>
    </div>

@endsection


@section('content')

    <div class="kt-portlet">
        <div class="kt-portlet__body  kt-portlet__body--fit">
            <div class="row row-no-padding row-col-separator-lg">

                <div class="col-md-12 col-lg-12 col-xl-3">

                    <!--begin::New Users-->
{{--                    <div class="kt-widget24">--}}
{{--                        <div class="kt-widget24__details">--}}
{{--                            <div class="kt-widget24__info">--}}
{{--                                <h2 class="kt-widget24__title">--}}
{{--                                    عدد عمليات البحث--}}
{{--                                </h2>--}}
{{--                            </div>--}}
{{--                            <span class="kt-widget24__stats kt-font-success"> {{ $logCount }}</span>--}}
{{--                        </div>--}}
{{--                        <div class="progress progress--sm">--}}
{{--                            <div class="progress-bar kt-bg-success" role="progressbar" style="width: 90%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

                    <!--end::New Users-->
                </div>
            </div>
        </div>
    </div>

@endsection
