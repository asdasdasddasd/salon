<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAIiPJXATL1VQa0KhqL9eWDo834B6v9O2M"></script>
{{--<script src="{{ asset('frontAssets/js/google-map.js') }}"></script>--}}

<!-- inject:js -->
<script src="{{ asset('frontAssets/js/vendors/modernizr-3.7.1.min.js') }}"></script>
<script src="{{ asset('frontAssets/js/vendors/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('frontAssets/js/vendors/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('frontAssets/js/vendors/jquery.meanmenu.min.js') }}"></script>
<script src="{{ asset('frontAssets/js/vendors/slick.min.js') }}"></script>
<script src="{{ asset('frontAssets/js/vendors/jquery.event.move.js') }}"></script>
<script src="{{ asset('frontAssets/js/vendors/jquery.twentytwenty.js') }}"></script>
<script src="{{ asset('frontAssets/js/vendors/jquery.nice-select.js') }}"></script>
<script src="{{ asset('frontAssets/js/vendors/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('frontAssets/js/vendors/jquery.ajaxchimp.min.js') }}"></script>
<script src="{{ asset('frontAssets/js/vendors/jquery.fancybox.min.js') }}"></script>
<script src="{{ asset('frontAssets/js/vendors/jquery.nstslider.min.js') }}"></script>
<script src="{{ asset('frontAssets/js/vendors/scrollspy.min.js') }}"></script>
<script src="{{ asset('frontAssets/js/vendors/ScrollMagic.min.js') }}"></script>
<script src="{{ asset('frontAssets/js/jqueryValidate.min.js') }}"></script>
<script src="{{ asset('frontAssets/js/main.js') }}"></script>
