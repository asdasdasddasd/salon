<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="apple-touch-icon" href="{{ asset('frontAssets/images/favicon.png') }}">
<link rel="shortcut icon" href="{{ asset('/frontAssets/images/favicon.ico') }}">

<!-- CSS FILES HERE -->
<!-- inject:css -->
<link rel="stylesheet" href="{{ asset('frontAssets/css/vendors/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontAssets/css/vendors/meanmenu.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontAssets/css/vendors/slick.css') }}">
<link rel="stylesheet" href="{{ asset('frontAssets/css/vendors/slick-theme.css') }}">
<link rel="stylesheet" href="{{ asset('frontAssets/css/vendors/themify-icons.css') }}">
<link rel="stylesheet" href="{{ asset('frontAssets/css/vendors/flaticon.css') }}">
<link rel="stylesheet" href="{{ asset('frontAssets/css/vendors/twentytwenty.css') }}">
<link rel="stylesheet" href="{{ asset('frontAssets/css/vendors/nice-select.css') }}">
<link rel="stylesheet" href="{{ asset('frontAssets/css/vendors/jquery.fancybox.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontAssets/css/vendors/jquery.nstSlider.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontAssets/css/style.css') }}">
<!-- endinject -->
{{ asset('frontAssets/') }}
