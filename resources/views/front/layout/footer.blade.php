<div class="tm-footer">

    <div class="tm-footer-toparea tm-padding-section"
         data-bgimage="{{ asset('frontAssets/images/footer-bgimage.jpg') }}"
         data-white-overlay="9">
        <div class="container">
            <div class="widgets widgets-footer row">

                <div class="col-lg-3 col-md-6 col-12">
                    <div class="single-widget widget-info">
                        <a class="widget-info-logo" href="index.html"><img
                                src="{{ asset('frontAssets/images/logo.png') }}"
                                alt="white logo"></a>
                        <p>
                            هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص
                            العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف
                            التى يولدها التطبيق.
                        </p>
                        <ul>
                            <li><a href="{{ getSetting('facebook') }}"><i class="ti ti-facebook"></i></a></li>
                            <li><a href="{{ getSetting('twitter') }}"><i class="ti ti-twitter-alt"></i></a></li>
{{--                            <li><a href="#"><i class="ti ti-pinterest"></i></a></li>--}}
{{--                            <li><a href="#"><i class="ti ti-linkedin"></i></a></li>--}}
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-12">
                    <div class="single-widget widget-quicklinks">
                        <h4 class="widget-title">Quick Links</h4>
                        <ul>
                            <li>
                                <a href="@if (\Request::is('/')) #tm-about-area @else {{ url('/') }}#tm-about-area @endif">من
                                    نحن</a></li>
                            <li>
                                <a href="@if (\Request::is('/')) #tm-service-area @else {{ url('/') }}#tm-service-area @endif">خدماتنا</a>
                            </li>
                            <li><a href="{{ route('front.offers') }}">العروض</a></li>
                            <li>
                                <a href="@if (\Request::is('/')) #tm-contactus-area @else {{ url('/') }}#tm-contactus-area @endif">تواصل
                                    معنا</a></li>

                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-12">
                    <div class="single-widget widget-hours">
                        <h4 class="widget-title">Opening Hours</h4>
                        <p>Lorem ipsum dolor sit amet, consect
                            adipisicing elit, sed do eiusmod tempor inci-didunt.</p>
                        <ul>
                            <li><span>Mon - Tue</span>: 9.00 AM - 18.00 PM</li>
                            <li><span>Wed - Fri</span>: 8.00 AM - 17.00 PM</li>
                            <li><span>Saturday</span>: 9.00 PM - 15.00 PM</li>
                            <li><span>Sunday</span>: Closed</li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="tm-footer-bottomarea">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <p class="tm-footer-copyright">CopyRights © {{ now()->year }}</p>
                </div>
                {{--                <div class="col-md-5">--}}
                {{--                    <div class="tm-footer-payment">--}}
                {{--                        <img src="{{ asset('frontAssets/images/payment-methods.png') }}" alt="payment methods">--}}
                {{--                    </div>--}}
                {{--                </div>--}}
            </div>
        </div>
    </div>
</div>
