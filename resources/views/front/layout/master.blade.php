<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>صالون</title>

    @include('front.layout.head')
</head>

<body>

<!-- Preloader -->
<div class="tm-preloader">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="tm-preloader-logo">
                    <img src="{{ asset('frontAssets/images/logo-white.png') }}" alt="logo">
                </div>
                <span class="tm-preloader-progress"></span>
            </div>
        </div>
    </div>
    {{--    <button class="tm-button tm-button-small">Cancel Preloader</button>--}}
</div>
<!--// Preloader -->

<!-- Wrapper -->
<div id="wrapper" class="wrapper">

    <!-- Header -->
@include('front.layout.header')

<!--// Header -->



@yield('content')


<!-- Footer -->
@include('front.layout.footer')
<!--// Footer -->


    <button id="back-top-top"><i class="ti ti-arrow-up"></i></button>

</div>
<!--// Wrapper -->

<!-- JS FILES HERE -->
@include('front.layout.scripts')
@stack('js')
<!-- endinject -->
</body>

</html>
