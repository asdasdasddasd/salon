<div id="tm-home-area" class="tm-header tm-header-sticky">

    <div class="tm-header-toparea">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <ul class="tm-header-info">
                        <li><i class="ti ti-mobile"></i><b>Contact:</b> <a href="tel:18009156270">1-800-915-6270
                            </a></li>
                        <li><i class="ti ti-time"></i><b>Work time:</b> 9:00-18:00</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="tm-header-icons">
                        <li><a href="#appointment-area" class="hash-scroll-link">Book Appointment</a></li>
                        <li><a href="cart.html"><i class="ti ti-shopping-cart"></i></a></li>
                        <li><a href="login-register.html"><i class="ti ti-user"></i></a></li>
                        <li><button class="tm-header-searchtrigger"><i class="ti ti-search"></i></button></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="tm-header-bottomarea">
        <div class="container">
            <div class="tm-header-bottominside">
                <div class="tm-header-searcharea">
                    <form action="#">
                        <input type="text" placeholder="Enter search keyword..">
                        <button type="submit"><i class="ti ti-search"></i></button>
                    </form>
                    <button class="tm-header-searchclose"><i class="ti ti-close"></i></button>
                </div>
                <div class="tm-header-inner">
                    <a href="index.html" class="tm-header-logo">
                        <img src="{{ asset('frontAssets/images/logo.png') }}" alt="munu">
                    </a>
                    <nav class="tm-header-nav">
                        <ul>
                            <li><a href="{{ url('/') }}">الرئيسية</a></li>

                            <li><a href="@if (\Request::is('/')) #tm-about-area @else {{ url('/') }}#tm-about-area @endif">من نحن</a></li>
                            <li><a href="@if (\Request::is('/')) #tm-service-area @else {{ url('/') }}#tm-service-area @endif">خدماتنا</a></li>

{{--                            --}}
{{--                            <li class="tm-header-nav-dropdown"><a href="#tm-news-area">Blog</a>--}}
{{--                                <ul>--}}
{{--                                    <li><a href="blog.html">Blog</a></li>--}}
{{--                                    <li><a href="blog-leftsidebar.html">Blog Left Sidebar</a></li>--}}
{{--                                    <li><a href="blog-details.html">Blog Details</a></li>--}}
{{--                                    <li><a href="blog-details-leftsidebar.html">Blog Details Left Sidebar</a>--}}
{{--                                    </li>--}}
{{--                                    <li><a href="blog-details-gallery.html">Blog Details Gallery</a></li>--}}
{{--                                    <li><a href="blog-details-audio.html">Blog Details Audio</a></li>--}}
{{--                                    <li><a href="blog-details-video.html">Blog Details Video</a></li>--}}
{{--                                </ul>--}}
{{--                            </li>--}}
                            <li><a href="@if (\Request::is('/')) #tm-contactus-area @else {{ url('/') }}#tm-contactus-area @endif">تواصل معنا</a></li>
                        </ul>
                    </nav>
                    <div class="tm-header-button">
                        <a href="@if (\Request::is('/')) #appointment-area @else {{ url('/') }}#appointment-area @endif" class="tm-button
                        @if (\Request::is('/')) hash-scroll-link @endif">حجز موعد</a>
                    </div>
                    <div class="tm-mobilenav"></div>
                </div>
            </div>
        </div>
    </div>

</div>
