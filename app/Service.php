<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Service extends Model
{
    protected $fillable = [
        'name' , 'description', 'image' , 'price'
    ];

    protected $appends = ['short_description'];

    public function getShortDescriptionAttribute()
    {
        return Str::limit( strip_tags($this->description) , 40);
    }

    public function getCreatedAtAttribute($created_at)
    {
        return Carbon::parse($created_at)->format('Y-m-d g:i A');
    }

    public function scopeFilter($q , $keySearch)
    {
        return $q->where('name' , 'like' , '%'.$keySearch . '%')
                    ->where('price' , 'like' , '%'.$keySearch . '%');
    }

}
