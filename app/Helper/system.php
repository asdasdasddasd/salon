<?php
use App\Setting;
use \Illuminate\Support\Str;
function getSetting($key){
    return Setting::getSetting($key)->value;
}

function string_limit($str , $limit , $endStr){
    return Str::limit($str , $limit , $endStr);
}
