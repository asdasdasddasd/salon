<?php

namespace App\Http\Controllers\Front;


use App\Offer;
use App\Service;
use App\Slider;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{

    public function home()
    {
        $data['sliders'] = Slider::where('isVisible' , 1)->get();
        $data['services'] = Service::all();
        $data['offers'] = Offer::latest()->take(3)->get();


        return view('front.index' , $data);
    }
//
//    public function setLanguage(Request $request)
//    {
//        $request['lang'] = $request->lang;
//        $lang = $request->lang;
//        $url = LaravelLocalization::getLocalizedURL($lang, url()->previous());
//        return Redirect::to($url);
//    }
}
