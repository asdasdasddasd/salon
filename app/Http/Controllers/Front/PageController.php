<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Page;
use Illuminate\Http\Request;
use Validator;

class PageController extends Controller
{

    public function whoWeAre()
    {
        $page = Page::find(1);
        return view('front.page' , compact('page'));
    }

    public function privacy()
    {
        $page = Page::find(2);
        return view('front.page' , compact('page'));
    }

    public function usagePolicy()
    {
        $page = Page::find(3);
        return view('front.page' , compact('page'));
    }
}
