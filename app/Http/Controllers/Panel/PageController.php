<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class PageController extends Controller
{
    private function niceNames()
    {
        return [
            'ar.title' => 'العنوان باللغة العربية',
            'en.title' => 'العنوان باللغة الانجليزية',
        ];

    }

    private function validation($data)
    {
        $validator = Validator::make($data, [
            'title' => 'required|string',
            'content' => 'required|string',
        ]);

        $validator->setAttributeNames($this->niceNames());

        return $validator;
    }

    public function index()
    {
        $pages = Page::all();
        return view('panel.pages.index', compact('pages'));
    }

    public function create()
    {
        return view('panel.pages.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $validator = $this->validation($data);


        if ($validator->fails()) {
            return response()->json([
                'status' => 422,
                'msg' => $validator->errors()->first()
            ], 422);
        }

        Page::create($data);
        return response()->json([
            'status' => 200,
            'msg' => 'تمت العملية بنجاح'
        ], 200);
    }

    public function edit($id)
    {
        $page = Page::findOrFail($id);
        return view('panel.pages.edit', compact('page'));
    }

    public function update(Request $request, $id)
    {
        $page = Page::findOrFail($id);
        $data = $request->all();
        $validator = $this->validation($data);


        if ($validator->fails()) {
            return response()->json([
                'status' => 422,
                'msg' => $validator->errors()->first()
            ], 422);
        }

        $page->update($data);
        return response()->json([
            'status' => 200,
            'msg' => 'تمت العملية بنجاح'
        ], 200);
    }

}
