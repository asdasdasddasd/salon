<?php

namespace App\Http\Controllers\Panel;

use App\Admin;
use App\Contact;
use App\Replay;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;

class ContactController extends Controller
{


    public function index()
    {
        Contact::query()->update([
            'read_at' => now()
        ]);
        return view('panel.contacts.index');
    }


    public function show($id)
    {
        $contact = Contact::findOrFail($id);
        return view('panel.contacts.show' , compact('contact'));
    }


    public function destroy($id , Request $request)
    {
        if ($request->json()){
            try{
                $contact = Contact::findOrFail($id);


                $contact->delete();

                return response()->json([
                    'status'    => 200 ,
                    'msg'       => 'تم حذف الرسالة بنجاح'
                ],200);
            }catch (\Exception $exception){
                return response()->json([
                    'status'    => 500 ,
                    'msg'       => 'لقد حدث خطأ ما'
                ] , 500);
            }
        }
    }

    public function datatable()
    {
        $pagination = Input::get('pagination');
        $query = Input::get('query');

        $data['name'] = $query['name'];
        $data['email'] = $query['email'];
        $data['phone'] = $query['phone'];

        $sort = Input::get('sort');


        if ($pagination['perpage'] == -1 || $pagination['perpage'] == null) {
            $pagination['perpage'] = 10;
        }
        $items = Contact::query();


        if ($data['name'] != null) {
            $items->where('name', 'like', '%' . $data['name'] . '%');
        }

        if ($data['email'] != null) {
            $items->where('email', 'like', '%' . $data['email'] . '%');
        }

        if ($data['phone'] != null) {
            $items->where('phone', 'like', '%' . $data['phone'] . '%');
        }


        if ($sort && count($sort)) {
            $items->orderBy($sort['field'], $sort['sort']);
        } else {
            $items->orderByDesc('created_at');
        }

        $itemsCount = $items->count();
        $items = $items->take($pagination['perpage'])->skip($pagination['perpage'] * ($pagination['page'] - 1))->get();
        $pagination['total'] = $itemsCount;
        $pagination['pages'] = ceil($itemsCount / $pagination['perpage']);

        $data['meta'] = $pagination;
        $data['data'] = $items;
        return $data;
    }

    public function reply(Request $request)
    {
        $data = $request->all();

        $niceNames = [
            'message' => 'الرد',
            'contact_id' => 'رسالة التواصل ',
        ];

        $validator = Validator::make($data, [
            'contact_id' => 'required|exists:contacts,id',
            'message' => 'required|string',
        ]);

        $validator->setAttributeNames($niceNames);

        if ($validator->fails()) {
            session()->flash('error', $validator->errors()->first());
            return back()->withInput();
        }

        Replay::create($data);
        session()->flash('success', 'تمت العملية بنجاح');
        return back();
    }

    public function showReplay($id)
    {

        $contact = Contact::with('replies')->findOrFail($id);
        return view('panel.contacts.replies' , compact('contact'));
    }
}
