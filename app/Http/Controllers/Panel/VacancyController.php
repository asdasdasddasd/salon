<?php

namespace App\Http\Controllers\Panel;

use App\Admin;
use App\Vacancy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class VacancyController extends Controller
{


    public function index(){
        return view('panel.vacancies.index');
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $validator = $this->validateRequest($data);

        if ($validator->fails()) {
            return response()->json([
                'status'    => 422 ,
                'msg'       => $validator->errors()->first()
            ],422);
        }

        Vacancy::create($data);
        return response()->json([
            'status'    => 200 ,
            'msg'   => 'تمت العملية بنجاح'
        ],200);

    }


    public function update(Request $request, $id)
    {
        $item = Vacancy::findOrFail($id);
        $data = $request->all();

        $validator = $this->validateRequest($data);

        if ($validator->fails()) {
            return response()->json([
                'status'    => 422 ,
                'msg'       => $validator->errors()->first()
            ],422);
        }

        $item->update($data);
        return response()->json([
            'status'    => 200 ,
            'msg'   => 'تمت العملية بنجاح'
        ],200);
    }


    public function deleteSelected(Request $request)
    {
        $ids = $request->ids;

        try {
            Vacancy::query()->whereIn('id', $ids)->delete();

            return response()->json([
                'status' => 200,
                'msg'   => 'تمت العملية بنجاح'
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => 500,
                'msg'   => 'لقد حدث خطأ ما'
            ], 500);
        }
    }



    public function datatable()
    {
        $pagination = Input::get('pagination');
        $query = Input::get('query');
        $search = $query['generalSearch'];



        if ($pagination['perpage'] == -1 || $pagination['perpage'] == null) {
            $pagination['perpage'] = 10;

        }
        $items = Vacancy::orderByDesc('created_at');


        if ($search != null) {
            $items = $items->filter($search);
        }

        $itemsCount = $items->count();
        $items = $items->take($pagination['perpage'])->skip($pagination['perpage'] *($pagination['page']-1) )->get();
        $pagination['total'] = $itemsCount;
        $pagination['pages'] = ceil($itemsCount / $pagination['perpage']);

        $data['meta'] = $pagination;
        $data['data'] = $items;
        return $data;
    }


    private function validateRequest($data,$id=null)
    {
        $validator = Validator::make($data, [
            'title' => 'required|string',
        ]);

        $validator->setAttributeNames($this->niceNames());
        return $validator;
    }

    private function niceNames()
    {
        return [
            'title' => 'عنوان الوظيفة',
        ];
    }
}
