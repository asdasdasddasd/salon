<?php

namespace App\Http\Controllers\Panel;

use App\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{
    private function niceNames()
    {
        return [
            'name' => 'الإسم',
            'description' => 'الوصف',
            'price' => 'السعر',
            'image' => 'الصورة',
        ];
    }



    public function index()
    {
        return view('panel.services.index');
    }

    public function create()
    {
        return view('panel.services.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required|string',
            'description' => 'required|string',
            'price' => 'required|numeric',
            'image' => 'required|image',
        ]);

        $validator->setAttributeNames($this->niceNames());

        if ($validator->fails()) {
            return response()->json([
                'msg' => $validator->errors()->first()
            ], 422);
        }

        if ($avatar = $request->file('image')){
            $data['image'] = $avatar->store('services');
        }

        Service::create($data);

        return response()->json([
            'msg' => 'تمت عملية الإضافة بنجاح'
        ], 200);
    }

    public function edit($id)
    {
        $data['service'] = Service::findOrFail($id);
        return view('panel.services.edit', $data);
    }


    public function update(Request $request, $id)
    {
        $service = Service::findOrFail($id);
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required|string',
            'description' => 'required|string',
            'price' => 'required|numeric',
            'image' => 'image',
        ]);

        $validator->setAttributeNames($this->niceNames());

        if ($validator->fails()) {
            return response()->json([
                'msg' => $validator->errors()->first()
            ], 422);
        }


        if ($image = $request->file('image')){
            Storage::delete($service->image);
            $data['image'] = $image->store('services');
        }


        $service->update($data);

        return response()->json([
            'msg' => 'تمت عملية التعديل بنجاح'
        ], 200);
    }

    public function destroy($id)
    {
        try{
            $service = Service::findOrFail($id);
            Storage::delete($service->image);
            $service->delete();
            return response()->json([
                'status' => 200,
                'msg' => 'لقد تمت عملية الحذف بنجاح'
            ], 200);
        }catch (\Exception $exception){
            return response()->json([
                'status' => 500,
                'msg' => 'لقد حدث خطأ ما'
            ], 500);
        }
    }


    public function datatable()
    {
        $pagination = Input::get('pagination');
        $query = Input::get('query');
        $search = $query['generalSearch'];

        $sort = Input::get('sort');


        if ($pagination['perpage'] == -1 || $pagination['perpage'] == null) {
            $pagination['perpage'] = 10;
        }

        $items = Service::query();

        if ($search) {
            $items->filter($search);
        }


        if ($sort && count($sort)){
            $items->orderBy($sort['field'] , $sort['sort']);
        }else{
            $items->orderByDesc('created_at');
        }

        $itemsCount = $items->count();
        $items = $items->take($pagination['perpage'])->skip($pagination['perpage'] * ($pagination['page'] - 1))->get();
        $pagination['total'] = $itemsCount;
        $pagination['pages'] = ceil($itemsCount / $pagination['perpage']);

        $data['meta'] = $pagination;
        $data['data'] = $items;
        return $data;
    }
}
