<?php

namespace App\Providers\Panel;

use App\Contact;
use App\Student;
use App\Teacher;
use App\TeacherPayment;
use App\VisitorMessage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class LayoutProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('panel.layout.master', function ($view) {
            $data['newMessage'] = Contact::query()->where('read_at',null)->count();
            $view->with($data);
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
