<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});
Route::get('image/{folder}/{path}/{size?}', 'Controller@photo');
Route::get('sliders/{folder}/{path}/{size?}', 'Controller@sliders');

Route::get('/lang', 'HomeController@setLanguage')->name('setLanguage');


Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
                , 'as' => 'front.' , 'namespace'=>'Front'], function () {

    Route::get('/', ['as' => 'home', 'uses' => 'HomeController@home']);
//    Route::get('/who-we-are', ['as' => 'whoWeAre', 'uses' => 'PageController@whoWeAre']);
//    Route::get('/privacy', ['as' => 'privacy', 'uses' => 'PageController@privacy']);
//    Route::get('/usage-policy', ['as' => 'usage-policy', 'uses' => 'PageController@usagePolicy']);


    Route::group(['prefix' => 'contact-us', 'as' => 'contact.'], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'ContactController@index']);
        Route::post('/', ['as' => 'send', 'uses' => 'ContactController@send']);
    });

    Route::get('/offers', ['as' => 'offers', 'uses' => 'OfferController@index']);


});
